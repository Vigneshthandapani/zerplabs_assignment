import React, { useEffect, useState } from 'react';
import "../Sass/components/MovieDetails.scss";

const MovieDetails = (props) => {
    const [movieDetails, setMovieDetails] = useState({})
    const [tableDataKey, setTableDataKey] = useState({});

    useEffect(() => {
        let tempTableDataKey = Object.keys(props.data).filter(e => {
            if (e != "Ratings" && e != "Title" && e != "Plot" && e != "Year" && e != "Poster" && e != "Response" && e != "Language") {
                return e;
            }
        });
        setTableDataKey(tempTableDataKey);
        setMovieDetails(props.data);
    }, [props]);

    const onClickCancel = () => {
        if (props.onClickCancel) {
            props.onClickCancel()
        }
    };

    return (
        <div class="modal">
            <div class="modal-content">
                <span id="close-icon" class="close" onClick={onClickCancel}>&times;</span>
                {Object.values(movieDetails).length > 0 &&
                    <div>
                        <div>
                            <img className="poster" src={movieDetails.Poster}></img>
                            <div className="info-container">
                                <h2>{movieDetails.Title}</h2>
                                <p className="mt-10px plot">{movieDetails.Plot}</p>
                                <p className="mt-10px mb-10px">{movieDetails.Year}</p>
                                <label className="mt-10px" class="pills">{movieDetails.Language}</label>
                                {movieDetails["Ratings"].length > 0 && <h3 className="mt-10px ratings-heading">Rating(s)</h3> }
                                <div className="ratingsContainer">
                                    {movieDetails["Ratings"].length > 0 && movieDetails["Ratings"].map((item) => {
                                        if (item.Source == "Internet Movie Database") {
                                            return <div className="ratings">
                                                <img src="/images/imdb.png" />
                                                <p>{item.Value}</p>
                                            </div>
                                        }
                                        else if (item.Source == "Rotten Tomatoes") {
                                            return <div className="ratings">
                                                <img src="/images/rotten_tomatoes.png" />
                                                <p>{item.Value}</p>
                                            </div>
                                        }
                                        else if (item.Source == "Metacritic") {
                                            return <div className="ratings">
                                                <img src="/images/metacritic.png" />
                                                <p>{item.Value}</p>
                                            </div>
                                        }
                                    })}
                                </div>
                            </div>
                        </div>
                        <h3 className="mt-20px">About the movie</h3>
                        <div className="tableContainer">
                            <table>
                                {tableDataKey.map((key) => {
                                    return <tr><td>{key}</td><td>{movieDetails[key]}</td></tr>
                                })}
                            </table>
                        </div>
                    </div>
                }
            </div>
        </div>
    );
};

export default MovieDetails;