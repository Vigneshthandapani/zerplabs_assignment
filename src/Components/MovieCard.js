import React, { useEffect, useState } from 'react';
import "../Sass/components/MovieCard.scss";

const MovieCard = (props) => {
    const[movie, setData] = useState({});
    
    useEffect(()=>{
        setData(props.data);
    }, [props])

    return (
        <div className="">
            <img className="card-image" src={movie.Poster}></img>
            <div className="text-container">
                <h3>{movie.Title}</h3>
                <p className="mt-10px">{movie.Year}</p>
            </div>
        </div>
    );
};

export default MovieCard;