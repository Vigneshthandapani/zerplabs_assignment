import React from 'react';
import "../Sass/components/NoResultFound.scss"

const NoResultFound = () => {
    return (
        <div>
            <div className="align-center ">
                <img className="noReultFoundImage" src="/images/no_result.svg"></img>
                <p className="text-center mt-20px">Sorry, no results found!</p>
            </div>
        </div>
    );
};

export default NoResultFound;