import React from 'react';

const Loader = () => {
    return (
        <div className="align-center">
            <img src="/images/loader.gif"></img>
        </div>
    );
};

export default Loader;