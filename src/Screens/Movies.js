import { useState, useEffect } from "react";
import NoResultFound from "../Components/NoResultFound";
import MovieCard from "../Components/MovieCard";
import MovieDetails from "../Components/MovieDetails";
import Loader from "../Components/Loader";
import "../Sass/screens/Movie.scss";

const Movies = () => {
    const [searchedMoviesData, setSearchedMoviesData] = useState([]);
    const [isNoResult, setIsNoResult] = useState(false);
    const [searchKeyword, setSearchKeyword] = useState(null);
    const [showMovieDetails, setShowMovieDetails] = useState(false);
    const [selectedMovieDetails, setSelectedMovieDetails] = useState({});
    const [showLoader, setShowLoader] = useState(false);
    const [showError, setShowError] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");

    const searchMoviesByTitle = () => {
        setShowError(false);
        setErrorMessage("");
        setShowLoader(true);
        setIsNoResult(false);
        setSearchedMoviesData([])
        let url = "https://www.omdbapi.com/?apikey=7469759a&s=" + searchKeyword;
        fetch(url).then((responseJSON) => {
            return responseJSON.json()
        }).then((response) => {
            if (response.Response == "True") {
                if (response.Search.length > 0) {
                    setSearchedMoviesData(response.Search)
                }
            }
            else {
                setSearchedMoviesData([])
                setIsNoResult(true);
            }
            setShowLoader(false)
        }).catch((error) => {
            setErrorMessage("Something went wrong, Please try again...")
            setShowError(true);
            setShowLoader(false);
        });
    }

    const getMovieDetailsById = (id) => {
        setShowError(false);
        setErrorMessage("");
        setShowLoader(true);
        let url = "https://www.omdbapi.com/?apikey=7469759a&i=" + id;
        fetch(url).then((responseJSON) => {
            return responseJSON.json()
        }).then((response) => {
            if (Object.values(response).length > 0) {
                document.body.style.overflow = "hidden";
                setShowMovieDetails(true);
                setSelectedMovieDetails(response);
            }
            else{
                setErrorMessage("Something went wrong, Please try again...")
                setShowError(true);
            }
            setShowLoader(false);
        }).catch((error) => {
            setErrorMessage("Something went wrong, Please try again...")
            setShowError(true);
            setShowLoader(false);
        });
    }

    const onClickCancel = () => {
        document.body.style.overflow = "auto";
        setShowMovieDetails(false)
    }

    const onClickSearch = () => {
        if(searchKeyword != "" && searchKeyword != null){
            searchMoviesByTitle()
        }
    }

    return (
        <div>
            {showLoader && <Loader />}
            <h1 className="text-center">Movie DB</h1>
            <div className="searchBarContainer">
                <input className="searchInput" defaultValue={searchKeyword} placeholder="Enter movie title to search..."
                    onBlur={(e) => {
                        if (e.target.value != "")
                            setSearchKeyword(e.target.value)
                        else
                            setSearchKeyword(null);
                    }} 
                />
                <button className="searchButton" onClick={()=>{onClickSearch()}}> Search </button>
                {searchKeyword == null && searchedMoviesData.length == 0 &&
                    <div className="align-center">
                        <img className="searchImage" src="/images/movie.svg"></img>
                        <p className="mt-30px">Start searching movies!!!</p>
                    </div>
                }
            </div>
            {showError &&
                <div className="errorContainer">
                    <p>{errorMessage}</p>
                </div>
            }

            {searchedMoviesData.length > 0 &&
                <div className="moviesContainer">
                    <div>
                        <h2 className="d-inline-block align-middle ml-10px">Search Result(s) </h2>
                        <span className="d-inline-block align-middle">&nbsp;-&nbsp;{searchedMoviesData.length}</span>
                    </div>
                    {searchedMoviesData.map((item) => {
                        return <div className="card" onClick={() => getMovieDetailsById(item.imdbID)}>
                            <MovieCard data={item} />
                        </div>
                    })}
                </div>
            }
            {isNoResult && <div><NoResultFound /></div>}
            {showMovieDetails && <div><MovieDetails data={selectedMovieDetails} onClickCancel={() => { onClickCancel() }} /></div>}
        </div>
    );
};

export default Movies;